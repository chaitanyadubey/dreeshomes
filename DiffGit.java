
/*===========================================================================+
|   Copyright (c) 2018 Byte Bonding Technologies                            |
|                         All rights reserved.                              |
+===========================================================================+
|  HISTORY                                                                  |
|                                                                           |
|  Created by ChaitanyaDubey@gmail.com                                      |
|                                                                           |
|                                                                           |
|                                                                           |
+===========================================================================*/
import java.io.*;
import java.util.*;

public class DiffGit
{

    /*
    *This flag showOnlyChanged, is used to direct the output, weather to only the different files (not the files that are identical)
    */
    public static boolean showOnlyChanged = true;


    public static ArrayList localFilesArray = new ArrayList();

    public DiffGit()
    {}
/* Please edit the followiing varriables with your specific details" */

    public static String MY_WORK_ROOT_DIR = "D:\\Spring_Tool_Suite\\workspace-sts-3.9.5.RELEASE\\DreesHomes\\src\\main";



    static void listContents( File dir )
    {
        // Assume that dir is a directory.  List
        // its contents, including the contents of
        // subdirectories at all levels.
        // System.out.println("Directory \"" + dir.getName() + "\"");

        String[] files;  // The names of the files in the directory.
        files = dir.list();
        for (int i = 0; i < files.length; i++) {
            File f;  // One of the files in the directory.
            f = new File(dir, files[i]);
            if ( f.isDirectory() ) {
                // Call listContents() recursively to
                // list the contents of the directory, f.
                listContents(f);
            }
            else {
                // For a regular file, just print the name, files[i].
                String absolutePathWorkFile = f.getAbsolutePath();
                //String absolutePathSourceControlFile = replaceAll(absolutePathWorkFile,MY_WORK_ROOT_DIR,SOURCE_CONTROL_ROOT_DIR);

                // System.out.println(absolutePathWorkFile);
                // System.out.println(absolutePathSourceControlFile);


                localFilesArray.add(absolutePathWorkFile);
                // System.out.println(absolutePathWorkFile);
            }
        }
    } // end listContents()


    public String executeCommand(String command)
    {


        Process child;
        InputStream x;
        String output="";

        System.out.println("Command = "+command);


        try {
            child = Runtime.getRuntime().exec(command);

        }
        catch( Exception e )
        {
            e.printStackTrace();
            System.out.println( "Create process fails:  " + e.getMessage() );
            return output;
        }

        try {
            x = child.getInputStream();

        }
        catch( Exception e )
        {
            e.printStackTrace();
            System.out.println( "InputStream fails:  " + e.getMessage() );
            return output;
        }

        long buff;

        try{
            while((buff = x.read()) != -1)
            {
                output+=String.valueOf((char)buff);

            }
        } catch(IOException e) { e.printStackTrace();}




        try{ x.close(); }
        catch ( Exception e )
        {
            e.printStackTrace();
            System.out.println( "datasteam close " + e.getMessage());
        }

        child.destroy();



        return output;



    }




    /**
     *
     *
     */
    public static void main( String args[] )
    {

        BufferedWriter out=null;

        DiffGit DiffGit  = new DiffGit();

        localFilesArray = new ArrayList();

        DiffGit.listContents(new File(MY_WORK_ROOT_DIR));


        String localFileName="";

        String result;


        int count =1; // total file count

        int diffCount =1; // only the diffcount

        int serialNumberOutput = 1; //Displays the sequence of the file in the output html.


        try {
            System.out.print("|");

            out = new BufferedWriter(new FileWriter("DiffGit.html"));

            out.write("<html>");

            out.write("<TITLE> Difference for files in LOCAL and REMOTE </TITLE>");


            out.write("<table border =10>");

            out.write("<th  bgcolor='#FFFFCC'><h4>SNo.</h4></th>");
            out.write("<th  bgcolor='#FFFFCC'><h4> -F-I-L-E-N-A-M-E-(S) </h4></th>");
            out.write("<th  bgcolor='#FFFFCC'><h4> -D-I-F-F-E-R-E-N-C-E- </h4></th>");


            for(int fileIndex=0;fileIndex<	 localFilesArray.size() ;fileIndex++)
            {


			  /*
                    Exceptions to be compared
			  */

                localFileName = (String)localFilesArray.get(fileIndex);


                String command = "git diff "+localFileName ;



                String diffResult = "";
                String diffResultHtmlFormated = "";


                diffResult = DiffGit.executeCommand(command);





                diffResultHtmlFormated = diffResult;


                diffResultHtmlFormated = replaceAll(diffResultHtmlFormated,"<","&lt;");




                diffResultHtmlFormated = replaceAll(diffResultHtmlFormated,">","&gt;");



                diffResultHtmlFormated = replaceAll(diffResultHtmlFormated,"\n","</br>");




/*
* Create HTML ROW for a file.
*/

                if(showOnlyChanged == true )
                {
                    if(!diffResult.equals(""))
                    {

                        out.write("<tr>");
                        out.write("<td width=1%>");   out.write("<B>"+(serialNumberOutput++)+".</B>");   out.write("</td>");
                        out.write("<td width=10%>");   out.write("<B>" + localFileName + "</B>" );   out.write("</td>");
                        out.write("<td width=90% nowrap=1>");   out.write("&nbsp;"+diffResultHtmlFormated+"");   out.write("</td>");
                        out.write("</tr>");

                    }

                }
                else if (showOnlyChanged == false )
                {
                    out.write("<tr>");
                    out.write("<td width=1%>");   out.write("<B>"+(serialNumberOutput++)+".</B>");   out.write("</td>");
                    out.write("<td width=10%>");   out.write("<B> "+localFileName+ "</B>");   out.write("</td>");
                    out.write("<td width=90% nowrap=1>");   out.write("&nbsp;"+diffResultHtmlFormated+"");   out.write("</td>");
                    out.write("</tr>");

                }



                if(!diffResult.trim().equals("")){diffCount++;}


                count++;
                System.out.print(".");
            }//loop
            System.out.print("|");
            out.write("</table>");
            out.write("</html>");


            out.close();


        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(0);
        }

        System.out.flush();
        System.out.println( "\n- DiffGit - successful !!!"+
                "\n- "+(count-1)+" File(s) compared"+
                "\n- "+(diffCount-1)+" File(s) are Different"+
                "\n- OutputFile =  DiffGit.html \n");


    }


    public static String replaceAll(String str, String pattern, String replace)
    {
        int s = 0;
        int e = 0;

        StringBuffer result = new StringBuffer();
        while ((e = str.indexOf(pattern, s)) >= 0)
        {
            result.append(str.substring(s, e));
            result.append(replace);
            s = e+pattern.length();
        }
        result.append(str.substring(s));
        return result.toString();
    }
}