
REM : Note we are only deleting the war file, if tomcat is running it will automatically delete the folder associated with it
del /s /q C:\apache-tomcat-8.5.14\webapps\v1.war

REM : Note we are only copying the war file, if tomcat is running it will automatically explode this file and create the folder
xcopy /F /Y target\v1.war C:\apache-tomcat-8.5.14\webapps\