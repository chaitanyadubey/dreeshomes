-----------------------------------------------
To Create a new WAR file use the below command
-----------------------------------------------
mvnw.cmd clean install

-----------------------------------------------
To Deploy the WAR file use the below command
-----------------------------------------------
deploy_war.bat


--------------
GIT PUSH
-------------
Also Note that we commit the changes using the STS , but push need to do from the backend:

git push origin master

