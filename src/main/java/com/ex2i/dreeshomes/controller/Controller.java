package com.ex2i.dreeshomes.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ex2i.dreeshomes.dao.CustomerDAO;
import com.ex2i.dreeshomes.dao.HomeDAO;
import com.ex2i.dreeshomes.dao.LookupDAO;
import com.ex2i.dreeshomes.dao.UserDetailDAO;
import com.ex2i.dreeshomes.dao.ConstructionPhaseDAO;
import com.ex2i.dreeshomes.dao.ConstructionStepDAO;
import com.ex2i.dreeshomes.dao.ConstructionStepImageDAO;
import com.ex2i.dreeshomes.model.Customer;
import com.ex2i.dreeshomes.model.Document;
import com.ex2i.dreeshomes.model.Home;
import com.ex2i.dreeshomes.model.Image;
import com.ex2i.dreeshomes.model.Notification;
import com.ex2i.dreeshomes.model.RestResponse;
import com.ex2i.dreeshomes.model.ConstructionPhase;
import com.ex2i.dreeshomes.model.ConstructionStep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@CrossOrigin
@RestController
@RequestMapping(value = "/api")
public class Controller {

 private String TAG = "B=B Controller";
	
	private static final Logger mLogger = LoggerFactory.getLogger(Controller.class);
	
	@Autowired
	CustomerDAO mCustomerDAO;

	@Autowired
	HomeDAO mHomeDAO;

	@Autowired
	ConstructionPhaseDAO mConstructionPhaseDAO;

	@Autowired
	ConstructionStepDAO mConstructionStepDAO;

	@Autowired
	ConstructionStepImageDAO mConstructionStepImageDAO;
	
	@Autowired
	LookupDAO mLookupDAO;
	
	@Autowired
	UserDetailDAO mUserDetailDAO;
	
	
	
	// ...................................Customer....................................
	@CrossOrigin
	@RequestMapping(value = "/customers")
	public RestResponse getCustomers() {

	 	mLogger.debug(TAG,"getCustomers()");
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mCustomerDAO.getCustomers(), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;
	}

	@CrossOrigin
	@RequestMapping(value = "/customer/{customerId}")
	public RestResponse getCustomer(@PathVariable("customerId") String customerId) {

		mLogger.debug(TAG,"getCustomer() customerId="+customerId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mCustomerDAO.getCustomer(customerId), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}
	
	@CrossOrigin
	@RequestMapping(value = "/customer/{customerId}/homes")
	public RestResponse getCustomerHomes(@PathVariable("customerId") String customerId) {

		mLogger.debug(TAG,"getCustomerHomes() customerId="+customerId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mCustomerDAO.getCustomerHomes(customerId), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}
  // ...................................Home....................................
	@CrossOrigin
	@RequestMapping(value = "/homes")
	public RestResponse getHomes() {
		
		mLogger.debug(TAG,"getHomes()");
		

		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mHomeDAO.getHomes(), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}

	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}")
	public RestResponse getHome(@PathVariable("homeId") String homeId) {

		mLogger.debug(TAG,"getHome() homeId="+homeId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mHomeDAO.getHome(homeId), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}

	// ...................................Construction Phase....................................
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/phases")
	public RestResponse getPhases(@PathVariable("homeId") String homeId) {

		mLogger.debug(TAG,"getPhases() homeId="+homeId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mConstructionPhaseDAO.getPhases(homeId), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}

	// ...................................Construction Step.....................................
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/phase/{phaseId}/steps")
	public RestResponse getSteps(@PathVariable("homeId") String homeId, @PathVariable("phaseId") String phaseId) {

		mLogger.debug(TAG,"getSteps() homeId="+homeId+" , phaseId="+phaseId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mConstructionStepDAO.getSteps(homeId, phaseId),
					RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}
	
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/phase/{phaseId}/step/{stepId}")
	public RestResponse getStep(@PathVariable("homeId") String homeId, @PathVariable("phaseId") String phaseId,@PathVariable("stepId") String stepId) {

		mLogger.debug(TAG,"getStep() homeId="+homeId+" , phaseId="+phaseId+", stepId="+stepId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mConstructionStepDAO.getStep(homeId, phaseId,stepId),
					RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}
	
	//Spring MVC Controller with pagination 
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/phase/{phaseId}/step/{stepId}/images",params = { "page", "size" },method=RequestMethod.GET)
	public RestResponse getStepImages(@PathVariable("homeId") String homeId, 
			@PathVariable("phaseId") String phaseId,
			@PathVariable("stepId") String stepId,
			@RequestParam( "page" ) int page, 
			@RequestParam( "size" ) int size) {

		mLogger.debug(TAG,"getStepImages() homeId="+homeId+" , phaseId="+phaseId+", stepId="+stepId+",page="+page+",size="+size);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mConstructionStepImageDAO.getStepImages(homeId, phaseId,stepId),
					RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;
	}

	//Spring MVC Controller without pagination 
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/phase/{phaseId}/step/{stepId}/images")
	public RestResponse getStepImages(@PathVariable("homeId") String homeId, 
			@PathVariable("phaseId") String phaseId,
			@PathVariable("stepId") String stepId
		) {

		mLogger.debug(TAG,"getStepImages() homeId="+homeId+" , phaseId="+phaseId+", stepId="+stepId);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mConstructionStepImageDAO.getStepImages(homeId, phaseId,stepId),
					RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;
	}
//....................................................................LOOKUP.......................................................................

	@CrossOrigin
	@RequestMapping(value = "/lookup/{lookupType}")
	public RestResponse getLookupValues(@PathVariable("lookupType") String lookupType) {

		mLogger.debug(TAG,"getLookupValues() lookupType="+lookupType);
		
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mLookupDAO.getLookupValues(lookupType), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}

	//....................................................................USER DETAILS.......................................................................
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/contacts")
	public RestResponse getHomeContacts(@PathVariable("homeId") String homeId) {

		mLogger.debug(TAG,"getHomeContacts() homeId="+homeId);
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(mUserDetailDAO.getHomeContacts(homeId), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;

	}
	
	//...................................................................NOTIFICATION......................................................................
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/notification")
	public RestResponse getHomeNotification(@PathVariable("homeId") String homeId) {

		mLogger.debug(TAG,"getHomeNotification() homeId="+homeId);
		
		RestResponse restResponse = null;
		try {
			restResponse = new RestResponse(new Notification(), RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;
	}
	
	//...................................................................DOCUMENTS......................................................................
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/documents")
	public RestResponse getHomeDocuments(@PathVariable("homeId") String homeId) {

		mLogger.debug(TAG,"getHomeDocuments() homeId="+homeId);
		
		RestResponse restResponse = null;
		try {
			ArrayList<Document> list = new ArrayList<Document>();
			
			Document doc1 = new Document();
			doc1.setDocumentId("1001");
			doc1.setUrl("https://www.cmu.edu/blackboard/files/evaluate/tests-example.xls");
			doc1.setName("Evaluation");
			doc1.setType("xls");
			doc1.setSize("16 KB");
			doc1.setPhase("1");
			doc1.setIsNew("Y");
			doc1.setUploadedDate(""+(new Date()));
			list.add(doc1);
			
			Document doc2 = new Document();
			doc2.setDocumentId("1002");
			doc2.setUrl("http://www.orimi.com/pdf-test.pdf");
			doc2.setName("orimi");
			doc2.setType("pdf");
			doc2.setSize("20.1 KB");
			doc2.setPhase("3");
			doc2.setIsNew("Y");
			doc2.setUploadedDate(""+(new Date()));
			list.add(doc2);
			
						
			restResponse = new RestResponse(list, RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;
	}
	
	
	//...................................................................IMAGES......................................................................
	@CrossOrigin
	@RequestMapping(value = "/home/{homeId}/images")
	public RestResponse getHomeImages(@PathVariable("homeId") String homeId) {

		mLogger.debug(TAG,"getHomeImages() homeId="+homeId);
		
		RestResponse restResponse = null;
		try {
			ArrayList<Image> list = new ArrayList<Image>();
			
			Image image1 = new Image();
			image1.setThumbnail("https://lid.zoocdn.com/645/430/6d5397fc6e7d016b31257c3ff0474f3d5af01f74.jpg");
			image1.setUrl("https://lid.zoocdn.com/645/430/6d5397fc6e7d016b31257c3ff0474f3d5af01f74.jpg");
			image1.setType("jpg");
			image1.setSize("40.1 KB");
			image1.setPhase("2");
			image1.setIsNew("N");
			image1.setUploadedDate(""+(new Date()));
			image1.setDescription("This is image1");
			list.add(image1);
			
			
			Image image2 = new Image();
			image2.setThumbnail("https://images.pexels.com/photos/106399/pexels-photo-106399.jpeg");
			image2.setUrl("https://images.pexels.com/photos/106399/pexels-photo-106399.jpeg");
			image2.setType("jpg");
			image2.setSize("9,09 MB");
			image2.setPhase("3");
			image2.setIsNew("Y");
			image2.setUploadedDate(""+(new Date()));
			image2.setDescription("This is image2");
			list.add(image2);
			
			
			
			
			restResponse = new RestResponse(list, RestResponse.STATUS_SUCCESS);
		} catch (Exception e) {
			restResponse = new RestResponse(null, RestResponse.STATUS_FAIL);
		}
		return restResponse;
	}
	
	
}
