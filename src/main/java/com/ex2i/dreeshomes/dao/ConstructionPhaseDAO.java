package com.ex2i.dreeshomes.dao;


import com.ex2i.dreeshomes.mapper.ConstructionPhaseRowMapper;
import com.ex2i.dreeshomes.model.ConstructionPhase;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ConstructionPhaseDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ConstructionPhaseDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ConstructionPhase> getPhases() {
        String sql = "SELECT CONSTRUCTION_PHASE_ID, HOME_ID,  STATUS, TITLE, ACTIVE,PHASE_CODE,DESCRIPTION, CREATE_DATE, UPDATE_DATE  from CONSTRUCTION_PHASE";
        RowMapper<ConstructionPhase> rowMapper = new ConstructionPhaseRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }


    public List getPhases(String id) {
        String sql = "SELECT CONSTRUCTION_PHASE_ID, HOME_ID,  STATUS, TITLE, ACTIVE, PHASE_CODE, DESCRIPTION , CREATE_DATE, UPDATE_DATE  from CONSTRUCTION_PHASE where HOME_ID=" + id;
        RowMapper<ConstructionPhase> rowMapper = new ConstructionPhaseRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }


} // Ends PhaseDAO class