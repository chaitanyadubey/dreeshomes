package com.ex2i.dreeshomes.dao;

import com.ex2i.dreeshomes.mapper.ConstructionStepRowMapper;
import com.ex2i.dreeshomes.model.ConstructionStep;
import com.ex2i.dreeshomes.model.Customer;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ConstructionStepDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ConstructionStepDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

 
    public List getSteps(String homeId,String phaseId) {
        String sql = "select step.CONSTRUCTION_STEP_ID, step.CONSTRUCTION_PHASE_ID, step.COMPLETE_DATE, step.TITLE, step.STATUS, step.ACTIVE, step.CREATE_DATE, step.UPDATE_DATE from CONSTRUCTION_STEP step, HOME home , CONSTRUCTION_PHASE phase where home.HOME_ID = phase.HOME_ID and step.CONSTRUCTION_PHASE_ID = phase.CONSTRUCTION_PHASE_ID and home.HOME_ID = "+homeId+" and phase.CONSTRUCTION_PHASE_ID=  "+phaseId; 
        
        RowMapper<ConstructionStep> rowMapper = new ConstructionStepRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    
    public ConstructionStep getStep(String homeId,String phaseId,String stepId) {
        String sql = "select step.CONSTRUCTION_STEP_ID, step.CONSTRUCTION_PHASE_ID, step.COMPLETE_DATE, step.TITLE, step.STATUS, step.ACTIVE, step.CREATE_DATE, step.UPDATE_DATE  from CONSTRUCTION_STEP step, HOME home , CONSTRUCTION_PHASE phase where home.HOME_ID = phase.HOME_ID and step.CONSTRUCTION_PHASE_ID = phase.CONSTRUCTION_PHASE_ID and home.HOME_ID = "+homeId+" and phase.CONSTRUCTION_PHASE_ID=  "+phaseId +" AND CONSTRUCTION_STEP_ID="+stepId; 
        
        RowMapper<ConstructionStep> rowMapper = new ConstructionStepRowMapper();
        List list = this.jdbcTemplate.query(sql, rowMapper);
        
        ConstructionStep step;
 	   if(list==null || list.size()==0)
 	   {
 		  step = null;  
 	   }
 	   else
 	   {
 		  step = (ConstructionStep) list.get(0);
 	   }
 	 
  	   return step;
  	   
  	   
    }
    
    

} // Ends PhaseDAO class
