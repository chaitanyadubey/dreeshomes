package com.ex2i.dreeshomes.dao;


import com.ex2i.dreeshomes.mapper.ConstructionStepImageRowMapper;
import com.ex2i.dreeshomes.model.ConstructionStepImage;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ConstructionStepImageDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ConstructionStepImageDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
 
    public List getStepImages(String homeId,String phaseId,String stepId) {
        String sql = "select STEP_IMAGE_ID, STEP_ID, THUMBNAIL, IMAGE_URL, CREATE_DATE, UPDATE_DATE, ACTIVE from CONSTRUCTION_STEP_IMAGES where STEP_ID="+stepId; 
        
        RowMapper<ConstructionStepImage> rowMapper = new ConstructionStepImageRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    

} // Ends PhaseDAO class