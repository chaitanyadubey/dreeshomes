package com.ex2i.dreeshomes.dao;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ex2i.dreeshomes.mapper.CustomerRowMapper;
import com.ex2i.dreeshomes.mapper.HomeRowMapper;
import com.ex2i.dreeshomes.model.Customer;
import com.ex2i.dreeshomes.model.Home;

import java.util.List;

@Transactional
@Repository
public class CustomerDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public CustomerDAO(JdbcTemplate jdbcTemplate) {
	  this.jdbcTemplate = jdbcTemplate;
    }
    

    public List<Customer> getCustomers() {
    	   String sql = "SELECT USER_ID, COMPLETE_NAME, EMAIL ,MOBILE_NUMBER , THUMBNAIL , ROLE_ID FROM USER_DETAIL";
    	   RowMapper<Customer> rowMapper = new CustomerRowMapper();
    	   return this.jdbcTemplate.query(sql, rowMapper);
 
    }

    public Customer getCustomer(String id) {
    	   String sql = "SELECT USER_ID, COMPLETE_NAME, EMAIL ,MOBILE_NUMBER , THUMBNAIL , ROLE_ID FROM USER_DETAIL where USER_ID="+id;
    	   RowMapper<Customer> rowMapper = new CustomerRowMapper();
    	   List list =  this.jdbcTemplate.query(sql, rowMapper);
    	   Customer customer;
    	   if(list==null || list.size()==0)
    	   {
    		   customer = null;  
    	   }
    	   else
    	   {
    		   customer = (Customer) list.get(0);
    	   }
    	 
     	   return customer;
    }

    public List<Home> getCustomerHomes(String id) {
 	   String sql = "select HOME_ID, USER_ID, DREESHOMES_JOB_ID, ADDRESS, DIVISION, AREA, GEO_ADDRESS, THUMBNAIL, PERCENTAGE_COMPLETE, ACTIVE, CREATE_DATE, UPDATE_DATE from HOME where USER_ID="+id;
 	   RowMapper<Home> rowMapper = new HomeRowMapper();
 	   return this.jdbcTemplate.query(sql, rowMapper);
 	   
 }

 
    
    
    
    
}
