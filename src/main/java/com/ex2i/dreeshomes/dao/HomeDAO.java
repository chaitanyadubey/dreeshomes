package com.ex2i.dreeshomes.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ex2i.dreeshomes.mapper.HomeRowMapper;
import com.ex2i.dreeshomes.model.Home;

import java.util.List;

@Transactional
@Repository
public class HomeDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public HomeDAO(JdbcTemplate jdbcTemplate) {
	  this.jdbcTemplate = jdbcTemplate;
    }
    
    public List<Home> getHomes() {
    	   String sql = "select HOME_ID, USER_ID, DREESHOMES_JOB_ID, ADDRESS, DIVISION, AREA, GEO_ADDRESS, THUMBNAIL, PERCENTAGE_COMPLETE, ACTIVE from HOME 	";
    	   RowMapper<Home> rowMapper = new HomeRowMapper();
    	   return this.jdbcTemplate.query(sql, rowMapper);
 
    }

    public Home getHome(String id) {
    	   String sql = "select HOME_ID, USER_ID, DREESHOMES_JOB_ID, ADDRESS, DIVISION, AREA, GEO_ADDRESS, THUMBNAIL, PERCENTAGE_COMPLETE, ACTIVE from HOME where HOME_ID="+id;
    	   RowMapper<Home> rowMapper = new HomeRowMapper();
    	   List list =  this.jdbcTemplate.query(sql, rowMapper);
    	   Home home;
    	   if(list==null || list.size()==0)
    	   {
    		  home = null; 
    	   }
    	   else
    	   {
    		   home = (Home) list.get(0);
    	   }
    	  
     	   return home;
    }

    
    
}
