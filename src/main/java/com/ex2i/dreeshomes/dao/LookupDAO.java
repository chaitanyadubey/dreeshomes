package com.ex2i.dreeshomes.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ex2i.dreeshomes.mapper.LookupRowMapper;
import com.ex2i.dreeshomes.model.Lookup;

import java.util.List;

@Transactional
@Repository
public class LookupDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public LookupDAO(JdbcTemplate jdbcTemplate) {
	  this.jdbcTemplate = jdbcTemplate;
    }
    
    public List<Lookup> getLookupValues(String lookupType) {
    	   String sql = "SELECT LOOKUP_TYPE,LOOKUP_CODE, MEANING,DESCRIPTION,ACTIVE FROM ALL_LOOKUPS WHERE LOOKUP_TYPE='"+lookupType+"' AND NVL(ACTIVE,'N') = 'Y'";
    	   RowMapper<Lookup> rowMapper = new LookupRowMapper();
    	   return this.jdbcTemplate.query(sql, rowMapper);
 
    }
    
}
