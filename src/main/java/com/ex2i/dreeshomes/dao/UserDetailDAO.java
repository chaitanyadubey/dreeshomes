
package com.ex2i.dreeshomes.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ex2i.dreeshomes.mapper.UserDetailRowMapper;
import com.ex2i.dreeshomes.model.UserDetail;

import java.util.List;

@Transactional
@Repository
public class UserDetailDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public UserDetailDAO(JdbcTemplate jdbcTemplate) {
	  this.jdbcTemplate = jdbcTemplate;
    }
    
    public List<UserDetail> getHomeContacts(String homeId) {
    	   String sql = "select user1.USER_ID, user1.ROLE_ID, user1.DESIGNATION, user1.DREESHOMES_ID, user1.MOBILE_NUMBER, user1.EMAIL, user1.COMPLETE_NAME, user1.THUMBNAIL, user1.ACTIVE, user1.CREATE_DATE, user1.UPDATE_DATE, mgr.HOME_ID from USER_DETAIL user1, HOME_MANAGER mgr where mgr.user_id = user1.user_id and mgr.HOME_ID = "+homeId;
    	   		
    	   RowMapper<UserDetail> rowMapper = new UserDetailRowMapper();
    	   return this.jdbcTemplate.query(sql, rowMapper);
 
    }
    
}


