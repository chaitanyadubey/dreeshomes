package com.ex2i.dreeshomes.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.ConstructionPhase;


public class ConstructionPhaseRowMapper implements RowMapper<ConstructionPhase>
{

    @Override
    public ConstructionPhase mapRow(ResultSet row, int rowNum) throws SQLException {
        ConstructionPhase phase = new ConstructionPhase();
        phase.setConstructionPhaseId(row.getString("CONSTRUCTION_PHASE_ID"));
        phase.setHomeId(row.getString("HOME_ID"));
        phase.setStatus(row.getString("STATUS"));
        phase.setTitle(row.getString("TITLE"));
        phase.setActive(row.getString("ACTIVE"));
        phase.setPhaseCode(row.getString("PHASE_CODE"));
        phase.setDescription(row.getString("DESCRIPTION"));
        phase.setCreateDate(row.getString("CREATE_DATE"));
        phase.setUpdateDate(row.getString("UPDATE_DATE"));
        return phase;
    }

}