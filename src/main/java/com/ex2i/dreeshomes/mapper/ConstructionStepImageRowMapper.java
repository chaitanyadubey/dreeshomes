package com.ex2i.dreeshomes.mapper;



import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.ConstructionStepImage;

public class ConstructionStepImageRowMapper implements RowMapper<ConstructionStepImage>
{

    @Override
    public ConstructionStepImage mapRow(ResultSet row, int rowNum) throws SQLException {
    	ConstructionStepImage image = new ConstructionStepImage();
      
    	image.setStepImageId(row.getString("STEP_IMAGE_ID"));
    	image.setStepId(row.getString("STEP_ID"));
    	image.setThumbnail(row.getString("THUMBNAIL"));
    	image.setImageUrl(row.getString("IMAGE_URL"));
    	image.setActive(row.getString("ACTIVE"));
    	image.setCreateDate(row.getString("CREATE_DATE"));
    	image.setUpdateDate(row.getString("UPDATE_DATE"));
    	
        return image;
    }

}

