package com.ex2i.dreeshomes.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.ConstructionStep;

public class ConstructionStepRowMapper implements RowMapper<ConstructionStep>
{

    @Override
    public ConstructionStep mapRow(ResultSet row, int rowNum) throws SQLException {
    	ConstructionStep step = new ConstructionStep();
      
    	step.setConstructionStepId(row.getString("CONSTRUCTION_STEP_ID"));
    	step.setConstructionPhaseId(row.getString("CONSTRUCTION_PHASE_ID"));
    	step.setCompleteDate(row.getString("COMPLETE_DATE"));
    	step.setTitle(row.getString("TITLE"));
    	step.setStatus(row.getString("STATUS"));
    	step.setActive(row.getString("ACTIVE"));
    	step.setCreateDate(row.getString("CREATE_DATE"));
    	step.setUpdateDate(row.getString("UPDATE_DATE"));
    	
        return step;
    }

}

