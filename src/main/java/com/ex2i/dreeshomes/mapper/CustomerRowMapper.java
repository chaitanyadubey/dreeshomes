package com.ex2i.dreeshomes.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.Customer;


public class CustomerRowMapper implements RowMapper<Customer>
{

	 @Override
	   public Customer mapRow(ResultSet row, int rowNum) throws SQLException {
		 Customer customer = new Customer();
		 customer.setID(row.getString("USER_ID"));
		 customer.setName(row.getString("COMPLETE_NAME"));
		 customer.setEmail(row.getString("EMAIL"));
		 customer.setMobileNumber(row.getString("MOBILE_NUMBER"));
		 customer.setThumbnail(row.getString("THUMBNAIL"));
		 customer.setRoleId(row.getString("ROLE_ID"));
		 return customer;
	   }
	 
}
