package com.ex2i.dreeshomes.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.Home;


public class HomeRowMapper implements RowMapper<Home>
{

	 @Override
	   public Home mapRow(ResultSet row, int rowNum) throws SQLException {
		 Home home = new Home();
		
		 home.setHomeId(row.getString("HOME_ID"));
		 home.setUserId(row.getString("USER_ID"));
		 home.setDeeshomesJobId(row.getString("DREESHOMES_JOB_ID"));
		 home.setAddress(row.getString("ADDRESS"));
		 home.setDivision(row.getString("DIVISION"));
		 home.setArea(row.getString("AREA"));
		 home.setGeoAddress(row.getString("GEO_ADDRESS"));
		 home.setThumbnail(row.getString("THUMBNAIL"));
		 home.setPercentageComplete(row.getString("PERCENTAGE_COMPLETE"));
		 home.setActive(row.getString("ACTIVE"));
			
		 return home;
	   }
	 
}

