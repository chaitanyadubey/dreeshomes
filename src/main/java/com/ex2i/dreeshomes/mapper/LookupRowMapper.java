package com.ex2i.dreeshomes.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.Lookup;


public class LookupRowMapper implements RowMapper<Lookup>
{

	 @Override
	   public Lookup mapRow(ResultSet row, int rowNum) throws SQLException {
		 Lookup lookup = new Lookup();
		
		 lookup.setLookupType(row.getString("LOOKUP_TYPE"));
		 lookup.setLookupCode(row.getString("LOOKUP_CODE"));
		 lookup.setMeaning(row.getString("MEANING"));
		 lookup.setDescription(row.getString("DESCRIPTION"));
		 lookup.setActive(row.getString("ACTIVE"));

		 return lookup;
	   }
	 
}


