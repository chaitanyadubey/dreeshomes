package com.ex2i.dreeshomes.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.ex2i.dreeshomes.model.UserDetail;



public class UserDetailRowMapper implements RowMapper<UserDetail>
{

	 @Override
	   public UserDetail mapRow(ResultSet row, int rowNum) throws SQLException {
		 UserDetail user = new UserDetail();
		
		 user.setUserId(row.getString("USER_ID"));
		 user.setRoleId(row.getString("ROLE_ID"));
		 user.setDesignation(row.getString("DESIGNATION"));
		 user.setDreeshomesId(row.getString("DREESHOMES_ID"));
		 user.setMobileNumber(row.getString("MOBILE_NUMBER"));
		 user.setCompleteName(row.getString("COMPLETE_NAME"));
		 user.setThumbnail(row.getString("THUMBNAIL"));
		 user.setActive(row.getString("ACTIVE"));
		 user.setCreateDate(row.getString("CREATE_DATE"));
		 user.setUpdateDate(row.getString("UPDATE_DATE"));

		 return user;
	   }
	 
}













