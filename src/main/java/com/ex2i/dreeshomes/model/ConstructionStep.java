package com.ex2i.dreeshomes.model;

public class ConstructionStep {

	String constructionStepId;
	String constructionPhaseId;
	String completeDate;
	String title;
	String status;
	String active;
	String createDate;
	String updateDate;
	
	public String getConstructionStepId() {
		return constructionStepId;
	}
	public void setConstructionStepId(String constructionStepId) {
		this.constructionStepId = constructionStepId;
	}
	public String getConstructionPhaseId() {
		return constructionPhaseId;
	}
	public void setConstructionPhaseId(String constructionPhaseId) {
		this.constructionPhaseId = constructionPhaseId;
	}
	public String getCompleteDate() {
		return completeDate;
	}
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
}
