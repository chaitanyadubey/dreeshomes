package com.ex2i.dreeshomes.model;

import javax.xml.bind.annotation.XmlRootElement;


public class Customer {
	
	String ID;
	String name;
	String email;
	String roleId;
	String thumbnail;
	String token;
	String mobileNumber;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Customer(String name, String email, String iD) {
		super();
		this.name = name;
		this.email = email;
		ID = iD;
	}
	
	public Customer() {}
	

	
}
