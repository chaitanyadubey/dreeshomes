package com.ex2i.dreeshomes.model;

public class Home {

	String homeId;
	String userId;
	String deeshomesJobId;
	String address;
	String division;
	String area;
	String geoAddress;
	String thumbnail;
	String percentageComplete;
	String active;
	
	
	public String getHomeId() {
		return homeId;
	}
	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeeshomesJobId() {
		return deeshomesJobId;
	}
	public void setDeeshomesJobId(String deeshomesJobId) {
		this.deeshomesJobId = deeshomesJobId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getGeoAddress() {
		return geoAddress;
	}
	public void setGeoAddress(String geoAddress) {
		this.geoAddress = geoAddress;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getPercentageComplete() {
		return percentageComplete;
	}
	public void setPercentageComplete(String percentageComplete) {
		this.percentageComplete = percentageComplete;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	
	
}
