package com.ex2i.dreeshomes.model;

public class RestResponse {
	
	public static final String STATUS_FAIL = "fail";
	public static final String STATUS_SUCCESS = "success";
	
	private String status;
	
	private Object data;

	public RestResponse() {
		this.status = STATUS_FAIL;
		this.data = null;
	}
	
	public RestResponse( Object data,String status) {
		this.status = status;
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
